let counterFactory = require('../counterFactory.js');

let result = counterFactory();
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());
console.log(result.decrement());

let r = counterFactory();
console.log(r.decrement());