let limitFunctionCallCount = require("../limitFunctionCallCount");

// let result = limitFunctionCallCount( _ => console.log("Callback function invoked"), 3);
let result = limitFunctionCallCount( _ => "Hello", 3);
console.log(result());
console.log(result());
console.log(result());
console.log(result());
console.log(result());
console.log(result());
console.log(result());
console.log(result());
console.log(result());
console.log(result());