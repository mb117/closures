let cacheFunction = require("../cacheFunction.js");

const helloCallback = () => "hello";
const sum = (a,b) => a + b;
const diff = (a,b) => a - b;
const sumThree = (a,b,c) => a + b + c;

let invokeCallback = cacheFunction( sumThree);
console.log(invokeCallback(1,2,3));
console.log(invokeCallback(1,2,3));

