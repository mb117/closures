// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(cb, n) {
    function invokeCallback() {
        if ( n > 0) {
            cb(...arguments);
            n--;

        } else {
            // can only be invoked n times
            return null;
        }
    }

    return invokeCallback;
}

module.exports = limitFunctionCallCount;