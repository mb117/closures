// Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.

function cacheFunction(cb) {
    let cache = {};

    function invokeCallback() {
        let argumentsForCallback = String([...arguments]);
        // console.log(argumentsForCallback);

        // invoke cb if arguments are not in cache and then store the result in the cache
        if ( argumentsForCallback in cache === false) {
            cache[argumentsForCallback] = cb(...arguments);
        } else {
            // return the cache result
            return cache[argumentsForCallback];
        }
        // console.log(cache);
    }

    return invokeCallback;
}

module.exports = cacheFunction;